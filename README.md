# Sisop Praktikum Modul 2 2023 Bs D03


## SOAL 1

Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

### 1a
 Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.
```
     char *url = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
     char *downloadName = "binatang.zip";
     int cek;

```
    kode diatas menginisiasi link download "url" dan nama zip file "binatang.zip"

```
 child = fork();
    if (child == 0) {
        char *args[] = {"wget", "-O", downloadName, url, NULL};
        execv("/bin/wget", args);

    }
    waitpid(child, &cek, 0);
    
    // Mengunzip File
    child = fork();
    if (child == 0) {
        char *args[] = {"unzip", downloadName, NULL};
        execv("/bin/unzip", args);
    }
    waitpid(child, &cek, 0);
```
char *args[] = {"wget", "-O", downloadName, url, NULL};
kode diatas berguna untuk melakukan pendownload file dengan perintah wget
execv("/bin/wget", args);
kode diatas berguna untuk melakukan pendownload file , sedangkan untuk "execv" berguna untuk mengeksekusi perintah "wget".
waitpid(child, &cek, 0);
berguna untuk menunggu proses anak untuk berakhir dan mendapatkan proses keluarannya.
Dalam melakukan pengunzipan file hampir sama dengan mendownload file namun perintah yang di gunakan yaitu "unzip"

### 1b
  Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

```
 printf("\n\n-- SOAL B --\n");

    // Memilih gambar secara acak
    DIR *dir;
    
    struct dirent *ent;//memasukan perintah direct,menyimpan informasi file dalam fungsi ent
   
    char *dir_name = ".";//. berguna sebagai penunjuk alamt saat ini
    char **files = NULL;
    int gambar = 0;
    // Membuka direktori
    dir = opendir(dir_name);

    // Melakukan perhitungan jumlah gambar
    while ((ent = readdir(dir)) != NULL) {
        char *extension = strrchr(ent->d_name, '.');//strch berguna untuk mengembalikan pointer paling akhir
        if (extension != NULL && (strcmp(extension, ".jpg") == 0)) {
            gambar++;
        }
    }

    files = malloc(sizeof(char *) * gambar);
    rewinddir(dir);

    int i = 0;
    while ((ent = readdir(dir)) != NULL) {
        char *extension = strrchr(ent->d_name, '.');
        if (extension != NULL && (strcmp(extension, ".jpg") == 0)) {
            files[i] = strdup(ent->d_name);
            i++;
        }
    }

    closedir(dir);

    // Melakukan pemilihan gambar secara random
    if (gambar > 0) {
        srand(time(NULL));
        int index = rand() % gambar;
        printf("Gambar random yang terpilih : %s\n", files[index]);
    }
   //menghapus alokasi file random yang di pilih secara acak
    // free array of filenames
    for (int i = 0; i < gambar; i++) {
        free(files[i]);
    }

    free(files);
```
kode di atas untuk melakukan pemilihan secara random maka melakukan perhitungan jumlah gambar yang terdapat ada file zip. setelah jumlah gambar di dapatkan maka melakukan pemilihan gambar secara random hingga jumlah gambar nya lebih dari 0 dengan menggunakan "srand".lalu menghapus file yang kita pilih tadi dengan looping sebanyak jumlah gambar agar tidak memakan memori yang besar 

### 1c 
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

'''
  printf("\n-- SOAL C--\n");

    // Inisiasi pembuatan folder
    char *air = "HewanAir";
    char *darat = "HewanDarat";
    char *amphibi = "HewanAmphibi";
    //0777itu hak akses file nya
    if (mkdir(darat, 0777) == -1 || mkdir(amphibi, 0777) == -1 || mkdir(air, 0777) == -1) {
        fprintf(stderr, "Error: %s\n", strerror(errno));
    } else {
        printf("Directori succes\n");
    }

    // melakukan pemindahan file ke subdirektori
     char path_akhir[400];
    char path_awal[400];
    char unique_name[400];
    char target_dir[400];
    struct stat st;
    dir = opendir(".");

    // melakukan filtering file pada direktori
    while ((ent = readdir(dir)) != NULL) {
        sprintf(path_awal, "./%s", ent->d_name);

        // melakukan skip file yang tersembunyi
        if (ent->d_type == DT_DIR || ent->d_name[0] == '.') {
            continue;
        }
        // file amphibi
        else if (strstr(ent->d_name, "_amphibi") != NULL) {
            sprintf(path_akhir, "./HewanAmphibi/%s", ent->d_name);

            // pemindahan ke path baru
            rename(path_awal, path_akhir);
        }
       

        // file darat
        else if (strstr(ent->d_name, "_darat") != NULL) {
            sprintf(path_akhir, "./HewanDarat/%s", ent->d_name);

            // pemindahan ke path baru
            rename(path_awal,path_akhir);
        }

         // file air
        if (strstr(ent->d_name, "_air") != NULL) {
            sprintf(path_akhir, "./HewanAir/%s", ent->d_name);

            // pemindahan ke path baru
            rename(path_awal, path_akhir);
        }

        else {
            continue;
        }
    }

untuk menyelesaikan soal c maka program yang pertama melakukan pembuatan direktori sesuai perintah dengan di tandai direktori succes jika folder berhasil di buat. setelah itu melakukan pemfilteran file gambar ke subdirectori sesuai dengan nama file gambar tersebut dengan bantuan perinth "strstr".

### 1d 
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

```
printf("\n-- SOAL D --\n");

    // melakukan zip file ke directori yang di tentukan
    child = fork();
    if (child == 0) {
        char *args[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat/", NULL};
        execv("/bin/zip", args);
    }
    waitpid(child, &cek, 0);

    child = fork();
    if (child == 0) {
        char *args[] = {"zip", "-r", "HewanAir.zip", "HewanAir/", NULL};
        execv("/bin/zip", args);
    }
    waitpid(child, &cek, 0);

    child = fork();
    if (child == 0) {
        char *args[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi/", NULL};
        execv("/bin/zip", args);
    }
    waitpid(child, &cek, 0);

    // Menghilangkan directori yang tidak ter unzip
    child = fork();
    if (child == 0) {
        char *args[] = {"rm", "-r", "HewanDarat/", NULL};
        execv("/bin/rm", args);
    }
    waitpid(child, &cek, 0);

    child = fork();
    if (child == 0) {
        char *args[] = {"rm", "-r", "HewanAir/", NULL};
        execv("/bin/rm", args);
    }
    waitpid(child, &cek, 0);

    child = fork();
    if (child == 0) {
        char *args[] = {"rm", "-r", "HewanAmphibi/", NULL};
        execv("/bin/rm", args);
    }
    waitpid(child, &cek, 0);

    closedir(dir);
    return 0;
}
```
kode di atas melakukan proses pengezipan dengan membuat file zip-zip yang sesuai dengan nama file direktori yang kita buat yaitu hewandarat,hewaniar,hewanamphibi.lalu menghapus direktori yang tidak terunzip dengan perintah "rm"


## Nomor 2

### 1. Defines
```
#define _XOPEN_SOURCE 700

/*
Include <stdio.h>
Include ....
...
*/
#include <curl/curl.h>

#define GCC_PATH "/usr/bin/gcc"
#define MAKEFILE "mkfolder"
#define KILLER "killer"
#define hello "helloworld"

//Image download and zip setting
#define AMOUNT 15
#define DELAY 5
```
- _XOPEN_SOURCE 700 = Agar bisa menggunakan sigaction untuk menerima dan merespon/handling signal.

- #include <curl/curl.h> = Library curl untuk fungsi mengunduh. **Saat compiling, harus menambahkan `-lcurl` untuk linking library curl.**

- GCC_PATH = Absolute path lokasi program gcc untuk compile file .c.
- MAKEFILE = Nama file program pembuat folder.
- KILLER = Nama file killer.
- hello = Sisa debugging, file helloworld.
- AMOUNT = Jumlah gambar yang didownload per folder.
- DELAY = Waktu sebelum mendownload gambar selanjutnya.

### 2. Signal Handler
```
void userHandler(int signum){
    if (signum == SIGUSR1){
    kill(0, SIGKILL);
    }
    else if (signum == SIGUSR2){
    _exit(0);
    }
}
```
- Kegunaan : Fungsi untuk handling signal, mengirim perintah berbeda tergantung signal yang dikirim oleh KILLER ke proses utama. 
- Argument :
    - signum = Nilai signal yang diterima
- Proses :
    - Jika signal adalah SIGUSR1 = Membunuh proses parent dan child dengan mengirim SIGTERM ke semua proses dalam process group.
    - Jika signal adalah SIGUSR2 = Membunuh proses parent tanpa membunuh proses child, hanya meminta proses parent untuk exit.
- **NOTE = SIGUSR1 saat ini tidak terpakai.** `kill(0, SIGKILL)` seharusnya mengirim signal untuk semua proses dalam process group termasuk child, namun tidak berhasil dalam signal handler.
### 3. runProg
```
int runProg(char name[], char arg1[], char arg2[]){

    //Calls exec in child process
    wait(NULL);
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        char *argv[] = {name, arg1, arg2, NULL};
        execv(argv[0], argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()");
                abort();
            }
        } while (status > 0);
    }

    return(0);
}
```
- Kegunaan : Mengeksekusi program dengan absolute path yang diberikan beserta argumen yang diberikan. Fungsi utama adalah untuk mengeksekusi program pembuat folder.
- Argument :
    - name = Absolute path file yang dieksekusi
    - arg1 = Argumen 1 program
    - arg2 = Argumen 2 program
- Proses :
    - Membuat child process, child process mengeksekusi program.
    - Parent process menunggu hingga child process selesai.
### 4. compileNew
```
int compileNew(char name[], char * code){
    
    //Creates .c file
    int len = strlen(name);
    char * filename = malloc(len*2);
    strcpy(filename, name);
    strcat(filename, ".c");
    
    FILE * fp = fopen(filename, "w");
    //Checks for failure
    if(!fp){
        printf("ERROR : Failed to open %s (%s)\n", filename, strerror(errno));
        return 1;
    }
    //printf("%s : Writing\n", code);
    rewind(fp);
    fprintf(fp, "%s", code);
    fclose(fp);

    //Writing process done in child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        //Writes code to .c file

        //Compiles code
        char *argv[] = {"gcc", "-o", name, filename, NULL};
        execv(GCC_PATH, argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()\n");
                abort();
            }
        } while (status > 0);
    }
    free(filename);
    return 0;

}
```
- Kegunaan : Diberikan absolute path untuk file hasil output dan kode, membuat dan menuliskan kode ke dalam file .c lalu memanggil gcc untuk compile kode menjadi file sesuau path output.
- Argument :
    - name = Absolute path file hasil compilation
    - code = Kode dari program yang ingin dicompile.
- Proses :
    - Menyimpan di variabel terpisah nama file .c (absolute path file hasil compilation namun diberi extention ".c")
    - Membuka/membuat file .c (Keluar jika gagal membuka)
    - Menuliskan kode ke dalam file .c
    - Menghasilkan proses child untuk memanggil gcc
    - Child process : Memanggil gcc untuk mengcompile file .c
    - Parent process : Menunggu child process selesai lalu return
### 5. isDirEmpty
```
int isDirEmpty(char path[]){
  int n = 0;
  struct dirent *d;
  DIR *dir = opendir(path);
  if (dir == NULL) //Not a directory or doesn't exist
    return 2;
  while ((d = readdir(dir)) != NULL) {
    if(++n > 2)
      break;
  }
  closedir(dir);
  if (n <= 2) //Directory Empty
    return 1;
  else
    return 0;
}

int getDir(char parent[], char *res){
    //printf("Parent : %s\n", parent);
    int sen = 0;
    DIR *d;
    struct dirent *dir;
    d = opendir(parent);
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            struct stat attrib;
            char folderName[200];
            strcpy(folderName, parent);
            strcat(folderName, "/");
            strcat(folderName, dir->d_name);
            if(stat(folderName, &attrib)==0){
                if (S_ISDIR(attrib.st_mode))
                {
                    if(isDirEmpty(folderName)){
                        sen = 1;
                        strcpy(res, folderName);
                        break;
                    }
                }
            }
        }
        closedir(d);
        return sen;
    }
}
```
- Kegunaan : Mendeteksi apakah suatu directory kosong atau tidak/bukan directory.
- Argument :
- path = Absolute path folder yang ingin diperiksa.
- Proses :
    - Membuka directory sebagai struct dirent.
    - Menavigasi directory menggunakan dirent, apabila kosong maka hanya membaca "." dan ".."
    - Menghitung jumlah yang dibaca, jika lebih dari 2 maka ada file didalam directory (langsung keluar dari loop dan return 0).
- Return :
    - 1 = Directory kosong
    - 0 = Directory ada isi
    - 2 = Jika bukan directory atau tidak ada directory sesuai path.
### 6. getDir
```
int getDir(char parent[], char *res){
    //printf("Parent : %s\n", parent);
    int sen = 0;
    DIR *d;
    struct dirent *dir;
    d = opendir(parent);
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            struct stat attrib;
            char folderName[200];
            strcpy(folderName, parent);
            strcat(folderName, "/");
            strcat(folderName, dir->d_name);
            if(stat(folderName, &attrib)==0){
                if (S_ISDIR(attrib.st_mode))
                {
                    if(isDirEmpty(folderName)){
                        sen = 1;
                        strcpy(res, folderName);
                        break;
                    }
                }
            }
        }
        closedir(d);
        return sen;
    }
}
```
- Kegunaan : Mendapatkan absolute path dari suatu folder kosong yang terletak dalam suatu folder parent.
- Argument :
    - parent : Absolute path folder yang didalamnya ingin dicari apakah ada folder kosong atau tidak.
    - res : Pointer yang akan diisi absolute path folder kosong.
- Proses :
    - Membuka directory parent sebagai struct dirent
    - Untuk setiap file di dalam parent directory :
        - Menghasilkan absolute path file tersebut
        - Membuka informasi file sebagai struct stat
        - Jika file merupakan directory dan kosong, maka mereturn nilai 1
- Return :
    - 1 = Jika ditemukan folder kosong
    - 0 = Jika tidak ditemukan folder kosong
### 7. zipDir
```
int zipDir(char name[], char arg[]){
    //Calls exec in child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        printf("Zipping %s into %s (%d)\n", arg, name, getpid());
        char *argv[] = {"zip", "-rj", name, arg, NULL};
        execv("/usr/bin/zip", argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()");
                abort();
            }
        } while (status > 0);
    }

    return(0);
}
```
- Kegunaan : Zipping folder sesuai path yang diberikan.
- Argument :
    - name : Absolute path folder yang akan dizip
    - arg : Abolute path nama file .zip yang dihasilkan
- Proses :
    - Membuat child process
    - Child process : Menjalankan process "zip" menggunakan argumen name, arg dengan option -r (rekursif, zip semua file dalam directory) dan -j (Tidak zip folder penyusun absolute path seperti "home")
    - Parent process : Menunggu hingga child process selesai, lalu keluar.
### 8. delDir
```
int delDir(char arg[]){

    //Calls exec in child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        printf("Deleting %s (%d)\n", arg, getpid());
        char *argv[] = {"rm", "-rf", arg, NULL};
        execv("/usr/bin/rm", argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()");
                abort();
            }
        } while (status > 0);
    }

    return(0);
}
```
- Kegunaan : Menghapus folder sesuai path yang diberikan.
- Argument :
    - arg : Absolute path folder yang ingin dihapus.
- Proses :
    - Membuat child process
    - Child process : Menjalankan process "rm" menggunakan argumen arg dengan option -r (rekursif) dan -f (tidak mengirim prompt ke user)
    - Parent process : Menunggu hingga child process selesai, lalu keluar.
### 9. Main_Memeriksa Argumen
```
if(argc < 2){
    printf("ERROR : Usage is \"lukisan -(a or b)\"\n");
    return -1;
}
```
Memeriksa apakah user lupa memberi argumen -a/-b (jumlah argc kurang dari 2).
Proses :
- Apabila jumlah argumen kurang, proses keluar.
- Note : Pesan error digunakan saat debugging.
### 10.  Main_Memeriksa validitas argumen
```
int killtype;
if(strcmp(argv[1], "-a")==0){
    killtype = 1;
}
else if(strcmp(argv[1], "-b")==0){
    killtype = 0;
}
else{
    printf("ERROR : Argument must be between -a or -b\n");
    return -1;
}
```
Apabila user memberikan argumen, memeriksa apakah sesuai aturan atau tidak. Jika sesuai, maka mengatur jenis program killer.
Proses :
- Apabila argumen adalah "-a" : Mengatur killtype = 1 yang berarti program killer mengirim signal SIGUSR1
- Apabila argumen adalah "-b" : Mengatur killtype = 0 yang berarti program killer mengirim signal SIGUSR2
- Apabila argumen bukan keduanya, maka program keluar. Printf digunakan untuk debugging.
### 11. Main_Deklarasi dan Inisialisasi sigaction untuk signal handler
```
struct sigaction sa;
sa.sa_handler = userHandler;
```
### 12. Main_Kode untuk daemon dan untuk absolute path.
```
pid_t pid, sid;        // Variabel untuk menyimpan PID

//Stores program's directory absolute path 
//(PROSES 2 Start)
char *path = realpath(".", NULL);
char *ptr = realpath(".", NULL);
char *path1 = malloc(100);
strcpy(path1, path);
strcat(path1, "/");
//(PROSES 2 END)

pid = fork();     // Menyimpan PID dari Child Process

/* Keluar saat fork gagal
* (nilai variabel pid < 0) */
if (pid < 0) {
    exit(EXIT_FAILURE);
}

/* Keluar saat fork berhasil
* (nilai variabel pid adalah PID dari child process) */
if (pid > 0) {
    exit(EXIT_SUCCESS);
}

umask(0);

sid = setsid();
if (sid < 0) {
    exit(EXIT_FAILURE);
}

if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
}

close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);
```
- Proses Utama : Untuk membuat daemon =
    - Membuat var untuk menyimpan process id dan session id
    - Menyimpan process id untuk mendeteksi error dan menutup process parent.
    - Untuk process child, diberi nilai umask 0 agar permission yang dimiliki sama dengan permission parent.
    - Membuat session id.
    - Mengganti running directory menjadi root
    - Menutup input, output, dan error
- Proses 2 : Untuk baris 258 dan 262, digunakan untuk menyimpan absolute path dari lokasi awal eksekusinya program yang digunakan untuk path membuat folder, program membuat folder, dan program killer.
### 13. Main_Menyimpan PID Proses Utama dan menghasilkan path kedua program sekunder.
```
pid_t mainPID = getpid();
char mPID[sizeof(pid_t)];
printf("lukisan.c's PID : %d\n", mainPID);
sprintf(mPID, "%d", mainPID); 
char * path2 = malloc(100);
strcat(path2, path1);

strcat(path1, MAKEFILE);
mkdir(path1, 0777);
strcat(path1, "/");
strcat(path1, MAKEFILE);

strcat(path2, KILLER);
printf("ORI : %s\nPath1 : %s\nPath2 : %s\n", path, path1, path2);
```
Proses :
- Menyimpan nilai pid dari proses utama.
- Membuat folder khusus untuk program **mkfolder**.
- Menghasilkan absolute path dari program pembuat folder dan program killer.
### 14. Main_Compile Mkfolder
```
compileNew(path1, "#include <sys/types.h> \n\
#include <signal.h> \n\
#include <sys/stat.h> \n\
#include <stdio.h> \n\
#include <stdlib.h> \n\
#include <fcntl.h> \n\
#include <errno.h> \n\
#include <unistd.h> \n\
#include <syslog.h> \n\
#include <string.h> \n\
#include <time.h> \n\
#define DELAY 30 \n\
int main(int argc, char *argv[]){ \
 \
pid_t pid, sid;\
printf(\"ORIGINAL PID : %d\\n\", getpid());\
pid = fork();\
if (pid < 0) { \
  exit(EXIT_FAILURE); \
} \
 \
if (pid > 0) { \
  printf(\"CHILD PID = %d\\n\", pid); \
  exit(EXIT_SUCCESS); \
} \
umask(0); \
 \
sid = setsid(); \
if (sid < 0) { \
exit(EXIT_FAILURE); \
} \
 \
char * path = malloc(100);\
strcpy(path, argv[2]); \
 \
if ((chdir(path)) < 0) { \
exit(EXIT_FAILURE); \
} \
printf(\"kekw\\n\"); \
close(STDIN_FILENO); \n\
close(STDOUT_FILENO); \n\
close(STDERR_FILENO); \
 \
while(1){ \
    printf(\"CHILD CUR PID : %d\\n\", getpid()); \
 \
    struct tm *tm; \
    time_t t; \
    char str_time[100]; \
 \
    t = time(NULL); \
    tm = localtime(&t); \
 \
    strftime(str_time, sizeof(str_time), \"%F_%T\", tm); \
    strcat(path, \"/\"); \
    strcat(path, str_time); \
    if(!kill(atoi(argv[1]), 0)){ \
        printf(\"makefile.c : Main process is still running\\n\"); \
    } \
    else{ \
        if(errno == ESRCH){ \
          printf(\"makefile.c : Main process no longer running\\n\"); \
          exit(0); \
        } \
        else{ \
            printf(\"ERROR : makefile.c FAILED TO DETECT IF MAIN PROCESS IS STILL ACTIVE\\n\"); \
        } \
    } \
    if(mkdir(path ,0777) == -1){ \
      printf(\"ERROR CREATING DIRECTORY! : %s\\n\", strerror(errno)); \
      exit(0); \
    } \
    printf(\"OLD : %s\\n\", path);\
    free(path);\
    char * path = malloc(100);\
    strcpy(path, argv[2]); \
    sleep(DELAY); \
} \
return 0; \
}");
```
Source code dipass sebagai suatu string. Agar mudah dibaca, kode tetap diberi '\n' namun ditambah '\\' agar tidak terbaca sehingga nyatanya dalam string, per baris tidak dipisahkan oleh '\n'.
### PENJELASAN SOURCE CODE
- Membuat sebagai daemon seperti pada proses utama.
- Menggunakan **tm**, **time_t**, dan fungsi **strftime**, mendapatkan waktu saat ini lalu bersama **strcat** membuat string berisi nama folder.
- Selama **PROSES UTAMA MASIH HIDUP** :
    - Membuat folder baru menggunakan **mkdir** sesuai path.
    - Bersiap untuk mendapatkan nama folder selanjutnya lalu menunggu selama 30 detik.
- Jika proses utama sudah tidak ada : Keluar.
### 15. Main_Run MakeFolder
```
runProg(path1, mPID, path);
free(path1);
char * code = malloc(1000);
```
Menjalankan program mkfolder dengan argumen : 
- mPID = pid proses utama (agar process bisa mendeteksi apakah process utama masih berjalan atau tidak)
- path = Path folder lokasi program utama (untuk memberi tahu lokasi untuk membuat folder-folder barunya.)

Juga dilakukan alokasi memori untuk menyimpan source code program killer.

### 16. Main_Compile Killer
```
sprintf(code, "#define _XOPEN_SOURCE 700\n\
#include <stdio.h> \n\
#include <stdlib.h>\n\
#include <signal.h>\n\
#include <sys/types.h>\n\
#include <errno.h>\n\
#include <string.h>\n\
#include <unistd.h>\n\
\n\
int main(int argc, char *argv[]){\
    pid_t pid =%d;\
    int killType = %d;\
    int signal;\
    if(killType){\
        signal = SIGTERM;\
    }\
    else{\
        signal = SIGUSR2;\
    }\
    if(!kill(-(getpgid(pid)), signal)){\
        printf(\"Process KILLED.\\n\");\
        char selfpath[1000];\
        getcwd(selfpath, sizeof(selfpath));\
        pid_t selfpid = fork();\
        if(selfpid == 0){\
            strcat(selfpath, \"/killer\");\
            remove(selfpath);\
            return 0;\
        }\
        else{\
            return 0;\
        }\
    }\
    else{\
        if(errno == ESRCH){\
            printf(\"No such signal exists\\n\");\
        }\
        else{\
            printf(\"ERROR : Failed to kill program\\n\");\
        }\
    }\
    return 0;\
}", mainPID, killtype);
    printf("ke\n");
    compileNew(path2, code);
    wait(NULL);
    free(path2);
    free(code);
```
Untuk source code program killer, terdapat 2 nilai yang harus tambah nilainya setiap kali dicompile oleh process utama. Yaitu : 
- pid process utama agar terdapat target untuk dikirim signal
- nilai **killType** yang menentukan signal yang dikirim.

Setelah nilai tersebut ditentukan dan diset menggunakan **sprintf**, source code dicompile lalu char pointer yang sebelumnya dialokasikan memori menggunakan malloc di bebaskan.
### Penjelasan Source Code
- Apabila **killtype** bernilai 1, maka program utama jalan dalam TYPE_A, sedangkan jika bernilai 0, maka TYPE_B.
- Signal akan dikirim ke semua program dalam process group proses utama, maka :
    - TYPE_A = Program mengirim **SIGTERM** agar seluruh proses child bersama proses utama berhenti.
    - TYPE_B = Program mengirim **SIGUSR2** agar proses utama menggunakan handler menghentikan dirinya, tapi proses child tetap berjalan.
- Program mendapatkan running directory.
- Program membuat child proses yang akan menghasilkan path program killer dan menghapusnya.

### 17. Main_Setup Child
```
//Generates child process when new dir is detected
while(1){
    /* Generates empty dir */
    char * folderPath = malloc(200);
    if(getDir(ptr, folderPath)){
        child_id = fork();

            if (child_id < 0) {
                exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
            }

            if (child_id == 0) {
                
                strcpy(ptr, folderPath);
```
Ini adalah bagian awal looping proses mengunduh dan mengzip dari proses utama.
Pertama, proses mencari absolute path folder kosong di directory lokasi program utama menggunakan fungsi "getDir". Jika ditemukan maka dibuat child_process sehingga akan terdapat child process untuk setiap folder kosong yang diproses.

### 18. PROSES DOWNLOAD
Selama ini, terdapat beberapa baris kode yang berfungsi untuk menyiapkan dan membersihkan proses download curl.
- A. write_data
```
static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}
```
Fungsi ini dipanggil untuk menuliskan data yang diunduh ke dalam file.
- B. Curl setup
```
CURL *curl_handle;
FILE *pagefile;

curl_global_init(CURL_GLOBAL_ALL);

/* init the curl session */
curl_handle = curl_easy_init();



/* Switch on full protocol/debug output while testing */
curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);

/* disable progress meter, set to 0L to enable it */
curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

/* Enables redirects */
curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);

/* Change user agent to firefox, avoids "error 1020" due to curl being blocked*/
curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0");

/* send all data to this function  */
curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);
```
Fungsi-fungsi ini menyiapkan CURL untuk mengunduh dan memproses data yang diunduh. Contohnya adalah **curl_easy_setopt** yang berfungsi menentukan user agent sebagai firefox karena user agent default CURL diblokir situs picsum.photos.
- C. Curl DOWNLOAD
```
for(int x = 0; x < AMOUNT; x++){
    //DEBUG
    printf("CHILD PID : %d\n", getpid());
    printf("CHILD GID : %d \n", getpgid(getpid()));
    
    /* Generates link*/
    printf("1 : %d\n", x);
    char mainLink[] = "https://picsum.photos/";
    char dim[6];
    sprintf(dim, "%u", ((unsigned)time(NULL)%1000)+50);
    strcat(mainLink, dim);
    strcat(mainLink, ".jpg");
    
    /* set URL to get here */
    printf("2 : %d\n", x);
    curl_easy_setopt(curl_handle, CURLOPT_URL, mainLink);
    printf("3 : %d\n", x);
    /* Gets datetime for file name */
    struct tm *tm; 
    time_t t; 
    char str_time[100]; 

    t = time(NULL); 
    tm = localtime(&t); 

    strftime(str_time, sizeof(str_time), "%F_%T", tm);
    printf("4 : %d\n", x); 
    /* Sets file name */
    char * pagefilename = malloc(100);
    strcpy(pagefilename, ptr);
    strcat(pagefilename, "/");
    strcat(pagefilename, str_time);
    strcat(pagefilename,".jpg");
    
    printf("%d ||\nLINK : %s\nFILENAME : %s\n", x, mainLink, pagefilename);
    /* open the file */
    pagefile = fopen(pagefilename, "wb");
    if(pagefile) {

        /* write the page body to this file handle */
        curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);

        /* get it! */
        curl_easy_perform(curl_handle);

        /* close the header file */
        fclose(pagefile);
    }
```
Proses ini terbagi menjadi beberapa tahap yang looping selama 15 kali :
- Program membentuk link yang akan diakses. Untuk dimensi foto, dibuat menggunakan **time(NULL)** untuk mendapatkan detik sejak epoch lalu dimodulo 1000 lalu ditambah 50.
- Program lalu memanggil **curl_easy_setopt** untuk menyimpan link yang diakses sesuai yang sudah ditentukan.
- Program lalu membentuk nama file .jpg untuk menyimpan data yang diunduh CURL menggunakan **tm**, **time_t**, dan **strftime**.
- Setelah itu, curl akan membuka file tersebut, lalu mengunduh dan menuliskan data kedalamnya sebelum menutup file tersebut.

Setelah semua gambar diunduh, maka akan dilakukan cleanup. 
- Curl Cleanup
```
/* cleanup curl stuff */
curl_easy_cleanup(curl_handle);

curl_global_cleanup();
```

### 19. Main_Zip, Delete Folder, dan Signal Handler
```
                   // /* Sets zip name */

                    char * zipName = malloc(200);
                    strcpy(zipName, ptr);
                    strcat(zipName,".zip");
                    zipDir(zipName, ptr);
                    delDir(ptr);
                    free(zipName);
                    free(folderPath);
                } else {
                    sleep(30);
                    sigaction(SIGUSR1, &sa, 0);
                    sigaction(SIGUSR2, &sa, 0);
                    printf("lukisan.c still running (%d)\n", getpid());
                    continue;
                }

        }
    }
    return 0;
}
```
- Zip dan Delete : Langkah ini dimulai dengan membuat string berisi absolute path zip, lalu memanggil fungsi "zipDir" untuk membuat zip folder berisi gambar. Proses lalu memanggil fungsi delDir untuk menghapus folder tersebut.
- Signal Handler : Signal handler menggunakan sigaction yang berjalan pada proses utama.

### KESULITAN
- i.	Jika lokasi path terlalu panjang, "curl" tidak bisa download file dengan benar. Proses akan dicoba sebelum error dan program berlanjut dimana hanya muncul 1 file corrupted.
- ii.	Kesulitan menggunakan strcpy dan strcat ketika menggunakan char pointer untuk menyimpan string vs menggunakan string literal. Contohnya ketika path folder utama tidak sengaja ke program untuk menghapus dan zip.
- iii.	Kesulitan untuk mengunduh file karena user agent default curl diblokir.
    1.	Solusi : curl_easy_setopt
- iv.	Kesulitan menyimpan isi program khusus makefolder dan killer di dalam kode file utama sebagai string terutama untuk program killer agar bisa mengirim signal berbeda tergantung dengan argumen file utama.
- v.	Kesulitan membedakan antara folder yang kosong dengan file lain. 
    1.	Solusi : Menggunakan readdir dan attrib.
- vi.	Kesulitan menampilkan arti pesan error.
    1.	Solusi : strerror
- vii.	Kesulitan cara handling pesan error
    1.	Solusi : sigaction
- viii.	Sigaction tidak mau mengirim kill ke seluruh process group


## Nomor 3

Program melakukan klasifikasi untuk mengenal para pemain Manchester United dengan Program C bernama filter.c

a. Program filter.akan mengunduh file yang berisikan database para pemain bola melalui link yang telah disediakan kemudian diminta melakukan extract players.zip.

```
#define zip "players.zip"
#define link "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"
```

kode diatas digunakan untuk mendefinisikan nama file zip dan link download.

```
pid_t child_id;

  child_id = fork();

  if (child_id == 0)
  {
    pid_t grand_child;
    grand_child = fork();
    if (grand_child == 0)
    {
      printf("Downloading.....\n\n");
      execl("/usr/bin/wget", "/usr/bin/wget", link, "-O", zip, NULL);
    }
    else
    {
      int status;
      wait(&status);
      printf("Unzipping.....\n\n");
      execl("/usr/bin/unzip", "/usr/bin/unzip", zip, "-d", ".", NULL);
    }
  }
  else
  {
    int status;
    wait(&status);
  }
```

Kode diatas adalah kode untuk membuat proses download dan unzipping. Child proses membuat proses baru untuk melakukan download, sedangkan child_id proses menunggu proses grand_child untuk selesai melakukan download kemudian melakukan unzip. Parent proses menunggu child proses selesai tereksekusi.

Kemudian untuk menghapus file zip digunakan kode berikut :

```
printf("removing zip file...\n\n");
  pid_t ziprm = fork();
  if (ziprm == 0)
  {
    char *args[] = {"rm", "players.zip", NULL};
    execvp("rm", args);
  }
  else
  {
    int status;
    wait(&status);
  }
```

child proses menggunakan fungsi remove dengan execvp() kemudian menghapus players.zip. Parent proses hanya menunggu child proses selesai kemudian melanjutkan program.

b. Semua pemain yang bukan dari manchester United akan dihapus dari directory. untuk melakukannya maka disini kami menggunakan kode berikut

```
pid_t remove = fork();
  if (remove == 0)
  {
    char *args[] = {"find", "players", "-maxdepth", "1", "-not", "-name", "*ManUtd*", "-type", "f", "-delete", NULL};
    execvp("find", args);
  }
  else
  {
    int status;
    wait(&status);
  }
```

Kode tersebut membuat proses baru dan menggunakan execvp untuk memanggil perintah find. Dengan parameter yang ada di dalam args, child proses akan menghapus semua file yang tidak berisi kata _ManUtd_ pada folder players. Parent proses menunggu child proses selesai kemudian melanjutkan program.

c. Instruksi mengharuskan program untuk mengkategorikan semua pemain sesuai dengan posisi dengan 4 proses pada waktu yang bersamaan.

```
printf("Making folders...\n\n");
  pid_t makedir = fork();
  if (makedir == 0)
  {
    char *args[] = {"mkdir", "players/Kiper", "players/Gelandang", "players/Penyerang", "players/Bek", NULL};
    execvp("mkdir", args);
  }
  else
  {
    int status;
    wait(&status);
  }
```

pertama proses baru dibuat untuk membuat keempat folder untuk klasifikasi. Kemudian kode berikut adalah kode untuk membuat 4 proses yang mengklasifikasi secara bersamaan.

```
printf("Classifying...\n\n");
  pid_t child1 = fork();
  if (child1 == 0)
  {
    pid_t child2 = fork();
    if (child2 == 0)
    {
      pid_t child3 = fork();
      if (child3 == 0)
      {
        pid_t child4 = fork();
        if (child4 == 0)
        {
          execl("/bin/sh", "sh", "-c", "mv players/*Bek* players/Bek/ 2>/dev/null", NULL);
        }
        else
        {
          execl("/bin/sh", "sh", "-c", "mv players/*Gelandang* players/Gelandang/ 2>/dev/null", NULL);
        }
      }
      else
      {
        execl("/bin/sh", "sh", "-c", "mv players/*Penyerang* players/Penyerang/ 2>/dev/null", NULL);
      }
    }
    else
    {
      execl("/bin/sh", "sh", "-c", "mv players/*Kiper* players/Kiper/ 2>/dev/null", NULL);
    }
  }
  else
  {
    int status;
    wait(&status);
  }
```

Child proses membuat proses baru lagi dan seterusnya hingga ada 4 proses selain parent proses pada program. Keempat proses tersebut digunakan untuk mengklasifikasikan pemain berdasarkan posisinya. Perintah yang digunakan adalah **mv**.  
Pada soal ini program awalnya mengalami masalah karena perintah tidak dapat dijalankan dengan execvp yang biasa kami gunakan. Untuk mengatasi hal tersebut kita menggunakan execl yang memanggil shell untuk menjalankan perintah.

d. Pada soal ini program diharuskan memiliki fungsi untuk membentuk kesebelasan terbaik berdasarkan rating pemain dan formasi yang digunakan. Karena kiper sudah pasti 1 orang, maka program meminta user untuk menginput 10 orang lainnya untuk membentuk formasi

```
int b, g, s;
  while (1)
  {
    printf("Masukkan urutan formasi(Bek, Gelandang, Penyerang): \n");
    scanf("%d %d %d", &b, &g, &s);
    // exception jika pemain yang diinput kurang dari 10
    if ((b + g + s) < 10)
    {
      printf("Jumlah pemain dalam formasi kurang!\n");
    }
    else if ((b + g + s) > 10)
    {
      printf("Jumlah pemain dalam formasi kelebihan!\n");
    }
    else
    {
      break;
    }
  }
  buatTim(b, g, s);
```

Program akan melakukan looping hingga input dari user benar (Total pemain berjumlah 10 orang). Kemudian memanggil fungsi **buatTim()** dengan 3 parameter integer.

Fungsi **buatTim()** digunakan untuk memasukkan nama pemain ke file txt yang dibuat sesuai dengan jumlah formasi yang diinputkan kemudian disimpan di folder home/user/

```
char filecreate[254];
  sprintf(filecreate, "/home/navahindra/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
  pid_t c_file = fork();
  if (c_file == 0)
  {
    char *args[] = {"touch", filecreate, NULL};
    execvp(args[0], args);
  }
  else
  {
    int status;
    wait(&status);
  }
```

Kode diatas berada di dalam fungsi **buatTim()** yang digunakan untuk membuat file berdasarkan jumlah formasi dengan format Formasi\_[jumlah bek]-[jumlah gelandang]-[jumlah striker].txt

```
// list kiper
  pid_t writing_kiper = fork();
  if (writing_kiper == 0)
  {
    pid_t child = fork();
    if (child == 0)
    {
      sprintf(filecreate, "echo 'Kiper:' >> /home/navahindra/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
    else
    {
      int status;
      wait(&status);
      sprintf(filecreate, "ls -1 players/Kiper/*.png | sort -n -r -t _ -k 4 | sed 's|players/Kiper/||g' | head -n %d >> /home/navahindra/Formasi_%d-%d-%d.txt", kiper, bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
  }
  else
  {
    int status;
    wait(&status);
  }
```

Kemudian kode diatas dibuat untuk membuat proses baru lalu mengambil player sesuai posisi dengan rating terbaik kemudian ditulis ke dalam txt file menggunakan **ls** kemudian **>>** ke file yang sudah dibuat.  
Untuk posisi yang lain kodenya serupa, dapat dilihat di filter.c

```
Kiper:
DeGea_ManUtd_Kiper_87.png
Bek:
Varane_ManUtd_Bek_85.png
Martinez_ManUtd_Bek_83.png
Shaw_ManUtd_Bek_82.png
Maguire_ManUtd_Bek_80.png
Gelandang:
Casemiro_ManUtd_Gelandang_89.png
Fernandes_ManUtd_Gelandang_86.png
Eriksen_ManUtd_Gelandang_83.png
Striker:
Rashford_ManUtd_Penyerang_84.png
Sancho_ManUtd_Penyerang_83.png
Antony_ManUtd_Penyerang_81.png
```

Output yang dikeluarkan contohnya seperti diatas dengan input formasi 4 3 3.


## Nomor 4

### PENJELASAN TEORI DIBALIK PROGRAM
Ketika kombinasi antara nilai angka dan nilai '*' dari jam, menit, dan detik diperiksa, maka ditemukan hasil berikut :

**Untuk ABC (Detik, menit, jam)**

**FORMAT PENGGUNAAN : `./mainan HH MM SS {.sh file path}`**

- *** = Execute once || Delay 1 second ||
- A** = Execute once || Delay 60 seconds
- AB* = Execute once || Delay 3600 seconds
- ABC = Execute once || Delay ONEDAY seconds

- \*B\* = Execute 60 times, delay 1 seconds || Delay 3600 seconds || (First execution) Executes while CURMIN = B
- *BC = Execute 60 times, delay 1 seconds || Delay ONEDAY seconds || (First execution) Executes while CURMIN = B, CURHOUR = C

- **C = Execute 3600 times, delay 1 seconds || Delay ONEDAY seconds
- A*C = Execute 60 times, delay 60 seconds || Delay ONEDAY seconds

Oleh karena itu, selama program mulai eksekusi di detik pertama waktu valid, jumlah eksekusi dan lama menunggu sebelum waktu valid selanjutnya bisa diprediksi.

Jumlah eksekusi, delay eksekusi, dan delay menunggu didefinisikan dalam program sebagai berikut :
- **repeat** = Jumlah eksekusi (misalnya **60 kali** dengan delay antar eksekusi 1 detik).
- **delayOn** = Lama menunggu jika eksekusi lebih dari sekali.
- **delayOff** = Lama menunggu untuk waktu valid selanjutnya. 

Maka, program hanya perlu memeriksa waktu valid setiap eksekusi jika program mulai eksekusi di tengah2 waktu valid (seperti di tengah jam valid untuk \*B\*).

### 1. Defines
```

/*
Include <stdio.h>
Include ....
...
*/
#define BASH "/usr/bin/bash"

#define ONESEC 1
#define ONEMIN 60
#define ONEHOUR 3600
#define ONEDAY 86400
```
- BASH = Absolute path lokasi program bash

- ONESEC, ONEMIN, ONEHOUR, dan ONEDAY mendefinisikan nilai detik untuk 1 detik, 1 menit, dst.

### 2. Fungsi "isValidTime"
```
int isValidTime(long int valHour, long int valMin, long int valSec, int anyH, int anyM, int anyS){
  time_t rawtime;
  struct tm * timeinfo;

  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  if(!anyH){
    if(timeinfo->tm_hour != valHour){
      return 0;
    }
  }
  if(!anyM){
    if(timeinfo->tm_min != valMin){
      return 0;
    }
  }
  if(!anyS){
    if(timeinfo->tm_sec != valSec){
      return 0;
    }
  }
  return 1;
}
```
- Kegunaan : Memeriksa apakah waktu saat fungsi dipanggil merupakan waktu valid untuk eksekusi program shell.
- Argument :
    - valHour, valMin, valSec = Nilai jam, menit, dan detik yang ingin dibandingkan dengan nilai sekarang. Diisi dengan nilai jam, menit, dan detik selanjutnya yang valid untuk eksekusi.
    - anyH, anyM, anyS = Nilai true (**1**) or false (**0**) untuk apakah jam, menit, atau detik bernilai **'*'** (nilai terserah).

- Return Value :
    - 0 = False, apabila terdapat nilai bukan '*' dimulai dari jam yang tidak sesuai dengan waktu valid.
    - 1 = True, apabila untuk jam, menit, dan detik bukan '*' waktu saat fungsi dipanggil valid untuk eksekusi.

### 3. Fungsi "execute"
```
int execute(char * path){
    pid_t child_id;
  //Gets absolute path
    //printf("%s %s %s %s\n", argv[1], argv[2], argv[3], argv[4]);
  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child

    char *arg[] = {"bash", path};
    printf("%s %s\n", arg[0], arg[1]);
    printf("EXECV ERROR %d : %s\n", execv(BASH, arg), strerror(errno));
    exit(0);
  } 
  else{
    int status;
    do {
        status = wait(NULL);
        if(status == -1 && errno != ECHILD) {
            perror("Error during wait()");
            abort();
        }
    } while (status > 0);
  }
    
    return 0;
  
}
```
- Kegunaan : Mengeksekusi program .sh sesuai dengan path yang diberikan.
- Argument :
    - path = String, absolute path dari file .sh yang ingin dieksekusi.
- Proses :
    - Memanggil fork
    - Untuk process child =
        - Membuat command menggunakan arg.
        - Mengeksekusi menggunakan execv.
    
    **!! Note : printf digunakan ketika debugging.**
    - Untuk process parent =
        - Menunggu hingga program child selesai sebelum keluar.

### 4. Fungsi "instruction"
```
void instruction(){
  printf("FORMAT : ./mainan HH MM SS {.sh file path}\n");
}
```
Kegunaan : Mencetak format penggunaan program.

### 5. Main_Setup Daemon
```
int main(int argc, char * argv[]) {
  pid_t pid, sid;        // Variabel untuk menyimpan PID

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  //close(STDOUT_FILENO);
  close(STDERR_FILENO);
```
- Membuat var untuk menyimpan process id dan session id.
- Menyimpan process id untuk mendeteksi error dan menutup process parent.
- Untuk process child, diberi nilai umask 0 agar permission yang dimiliki sama dengan permission parent.
- Membuat session id.
- Mengganti running directory menjadi root.
- Menutup stdin dan stderr.

### 6. Main_Memeriksa Argumen Program
```
  if(argc < 5 || argc > 5){
    printf("ERROR : WRONG ARG AMOUNT %d\n", argc);
    for(int x = 0; x < argc; x++){
      printf("ARG %d : %s\n", x, argv[x]);
    }
    instruction();
    return 0;
  }
```
Program memiliki 5 argumen dalam argv :
- Nama program
- Nilai crontab jam
- Nilai crontab menit
- Nilai crontab detik
- Abs path file .sh yang ingin dieksekusi	

Maka jika jumlah argumen kurang atau lebih dari 5, mencetak pesan error beserta argumen yang diberikan, lalu mencetak instruksi penggunaan program, terakhir program keluar.

### 7. Main_Memeriksa validitas argumen
```
  //Determines crontab input format
  int anyHour, anyMin, anySec;
  long int hour, min, sec;
  char *end;
  anyHour = anyMin = anySec = 0;
  if(strcmp(argv[1], "*")==0){
    anyHour = 1;
  }
  else{
    if(strcmp(argv[1], "0")==0){
      hour = 0;
    }
    //ERROR CHECK
    //else if
    else{
      hour = strtol(argv[1], &end, 10);
      printf("HOUR INPUT : %ld\n", hour);
      if(hour < 0 || hour > 23){
        printf("ERROR : Invalid HOUR amount\n");
        instruction();
        return 0;
      }
    }
  }
  if(strcmp(argv[2], "*")==0){
    anyMin = 1;
  }
  else{
    if(strcmp(argv[2], "0")==0){
      min = 0;
    }
    //ERROR CHECK
    //else if
    else{
      min = strtol(argv[2], &end, 10);
      printf("MINUTE INPUT : %ld\n", min);
      if(min < 0 || min > 59){
      printf("ERROR : Invalid MINUTE amount\n");
      instruction();
      return 0;
      }
    }
  }
  if(strcmp(argv[3], "*")==0){
    anySec = 1;
  }
  else{
    if(strcmp(argv[3], "0")==0){
      sec = 0;
    }
    //ERROR CHECK
    //else if
    else{
      sec = strtol(argv[3], &end, 10);
      printf("SECOND INPUT : %ld\n", sec);
      if(sec < 0 || sec > 59){
      printf("ERROR : Invalid SECOND amount\n");
      instruction();
      return 0;
    }
    }
  }
```
- Kegunaan :
    - Memeriksa apakah ada argumen yang tidak valid (Seperti nilai kurang atau lebih dari batas)
    - Mendeteksi keberadaan argumen '*' (terserah).
    - Menyimpan nilai jam, menit, dan detik sebagai angka.
- Proses :
	+ Untuk setiap if else :
        - Memeriksa apakah argumen bernilai '*'
        - Memeriksa apakah argumen bernilai 0. 
        - Mengubah argumen menjadi integer menggunakan strtol, lalu memeriksa batas minimum dan maksimum. Jika diluar batas nilai maka mencetak instruksi penggunaan dan keluar dari program.
	- If else 1# = Memeriksa argumen jam dengan batas nilai 0 - 23.
	- If else 2# = Memeriksa argumen menit dengan batas nilai 0 - 59.
	- If else 3# = Memeriksa argumen detik dengan batas nilai 0 - 59.

### 8. Main_Memeriksa apakah shell script bisa diakses.
```
  //Checks if file exists and can be accesed
  if (access(argv[4], F_OK) == 0){

  }
  else{
      printf("ERROR WHILE ACCESSING FILE : CODE %d (%s)\n", errno, strerror(errno));
      instruction();
      exit(EXIT_FAILURE);
  }
```
- Kegunaan :
    - Memeriksa apakah file shell script seperti yang diberikan dalam argumen bisa diakses atau tidak.
- Proses :
    - Memeriksa apakah file bisa diakses menggunakan fungsi "access".
    - Jika tidak bisa, mencetak pesan error beserta pesan error dari fungsi, mencetak instruksi penggunaan.

### 9. Main_Mengambil nilai waktu saat ini.
```
//Retrive current time and finds next valid trigger time
  time_t rawtime;
  struct tm * timeinfo;

  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
```
- Kegunaan :
    - Mengambil nilai waktu lokal saat ini.
- Proses :
    - Menyimpan waktu sebagai variabel time_t
    - Mengubah variabel time_t menjadi struct tm yang bisa diakses nilai individualnya (detik, menit, jam, dst).

### 10. Main_Menentukan nilai **repeat**, **delayOn**, dan **delayOff**.
```
  //Sets sleeping values
      //Delay stage
  int delayOff = 0;
    //Execute stage
  int repeat = 0;
  int delayOn = 0;

  if(!anyHour){
    delayOff = ONEDAY;
    if(!anyMin){
      if(!anySec){
        //HH MM SS
        repeat = 1;
        delayOn = 0;
      }
      else{
        //HH MM *
        repeat = 60;
        delayOn = 1;
      }
    }
    else{
      if(!anySec){
        //HH * SS
        repeat = 60;
        delayOn = 60;
      }
      else{
        //HH * *
        repeat = 3600;
        delayOn = 1;
      }
    }
  }
  else{
    if(!anyMin){
      delayOff = ONEHOUR;
      if(!anySec){
        //* MM SS
        repeat = 1;
        delayOn = 0;
      }
      else{
        //* MM *
        repeat = 60;
        delayOn = 1;
      }
    }
    else{
      repeat = 1;
      delayOn = 0;
      if(!anySec){
        //* * SS
        delayOff = ONEMIN;
      }
      else{
        //* * *
        delayOff = ONESEC;
      }
    }
  }
  
  //If enters in the middle of valid period
  int firstExec = 1;
```
Kode ini mengatur nilai **repeat**, **delayOn**, dan **delayOff** sesuai aturan yang dijelaskan di awal.

Di akhir, diinisialisasi variabel untuk menentukan apakah program dijalankan di tengah range waktu valid apabila menurut aturan crontab , jumlah eksekusi lebih dari sekali (selama menit X atau jam Y).

### 11. Main Delay
```
  float delay = 0;
  //Sets delay
  if(!anyHour){
      ti.tm_hour = hour;
      if(!anyMin){
        if(!anySec){
          //HH MM SS
          ti.tm_min = min;
          ti.tm_sec = sec;
          getDelay(rawtime, mktime(&ti), &delay, ONEDAY);
          sleep(delay);
        }
        else{
          //HH MM *
          if(timeinfo->tm_hour==hour && timeinfo->tm_min==min){

          }
          else{
            ti.tm_min = min;
            ti.tm_sec = 0;
            getDelay(rawtime, mktime(&ti), &delay, ONEDAY);


            sleep(delay);
          }
        }
      }
      else{
        if(!anySec){
          //HH * SS
          if(timeinfo->tm_hour!=hour){
            ti.tm_min = 0;
            ti.tm_sec = sec;
            getDelay(rawtime, mktime(&ti), &delay, ONEDAY);
            sleep(delay);
          }
          else{
            ti.tm_sec = sec;
            getDelay(rawtime, mktime(&ti), &delay, ONEMIN);
            sleep(delay);
          }
        }
        else{
          //HH * *
          if(timeinfo->tm_hour==hour){

          }
          else{
            ti.tm_min = 0;
            ti.tm_sec = 0;
            getDelay(rawtime, mktime(&ti), &delay, ONEDAY);


            sleep(delay);
          }
        }
      }
    }
    else{
      if(!anyMin){

        if(!anySec){
          //* MM SS
            ti.tm_min = min;
            ti.tm_sec = sec;
            getDelay(rawtime, mktime(&ti), &delay, ONEHOUR);
            sleep(delay);
        }
        else{
          //* MM *
          if(timeinfo->tm_min==min){

          }
          else{
            ti.tm_min = min;
            ti.tm_sec = 0;
            getDelay(rawtime, mktime(&ti), &delay, ONEHOUR);
            sleep(delay);
          }
        }
      }
      else{

        if(!anySec){
          //* * SS
            ti.tm_sec = sec;
            getDelay(rawtime, mktime(&ti), &delay, ONEMIN);
            sleep(delay);
        }
        else{
          //* * *

        }
      }
    }
```
Kode ini menentukan lama menunggu sebelum waktu valid selanjutnya dengan menentukan selisih waktu saat ini dengan waktu valid selanjutnya.

### 12. Main Loop Eksekusi .sh
```
  if(!delayOn){
    firstExec = 0;
  }

  while(1){
  //DEBUG

  //Execute
  
  printf("repeat = %d\ndelayOn = %d\ndelayOff = %d\n", repeat, delayOn, delayOff);
  for(int x = 0; x < repeat; x++){
    printf("PID : %d\n", getpid());
    execute(argv[4]);
      //Detects if still valid time
    if(firstExec){
      if(!isValidTime(hour, min, sec, anyHour, anyMin, anySec)){
        firstExec = 0;
        break;
      }
    }
    sleep(delayOn);
  }
  //Delay
  sleep(delayOff);
  }
  return 0;
}
```
Kode yang menjalankan file .sh sesuai aturan di awal. 

Apabila program mulai di tengah2 range waktu valid, program akan memeriksa setiap eksekusi apakah saat ini waktu valid atau tidak.

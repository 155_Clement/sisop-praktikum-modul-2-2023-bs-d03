#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>
#include <dirent.h>
#include <signal.h>
#include <curl/curl.h>

#define GCC_PATH "/usr/bin/gcc"
#define MAKEFILE "mkfolder"
#define KILLER "killer"
#define hello "helloworld"

//Image download and zip setting
#define AMOUNT 15
#define DELAY 5
//Signal Handler

void userHandler(int signum){
    if (signum == SIGUSR1){
    kill(0, SIGKILL);
    }
    else if (signum == SIGUSR2){
    _exit(0);
    }
}

//-------------------------------------------------------------

//Functions used to compile and run secondary c programs
int runProg(char name[], char arg1[], char arg2[]){

    //Calls exec in child process
    wait(NULL);
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        char *argv[] = {name, arg1, arg2, NULL};
        execv(argv[0], argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()");
                abort();
            }
        } while (status > 0);
    }

    return(0);
}
int compileNew(char name[], char * code){
    
    //Creates .c file
    int len = strlen(name);
    char * filename = malloc(len*2);
    strcpy(filename, name);
    strcat(filename, ".c");
    
    FILE * fp = fopen(filename, "w");
    //Checks for failure
    if(!fp){
        printf("ERROR : Failed to open %s (%s)\n", filename, strerror(errno));
        return 1;
    }
    //printf("%s : Writing\n", code);
    rewind(fp);
    fprintf(fp, "%s", code);
    fclose(fp);

    //Writing process done in child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        //Writes code to .c file

        //Compiles code
        char *argv[] = {"gcc", "-o", name, filename, NULL};
        execv(GCC_PATH, argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()\n");
                abort();
            }
        } while (status > 0);
    }
    free(filename);
    return 0;

}
//-------------------------------------------------------------
//Functions used to detect empty directories
int isDirEmpty(char path[]){
  int n = 0;
  struct dirent *d;
  DIR *dir = opendir(path);
  if (dir == NULL) //Not a directory or doesn't exist
    return 2;
  while ((d = readdir(dir)) != NULL) {
    if(++n > 2)
      break;
  }
  closedir(dir);
  if (n <= 2) //Directory Empty
    return 1;
  else
    return 0;
}

int getDir(char parent[], char *res){
    //printf("Parent : %s\n", parent);
    int sen = 0;
    DIR *d;
    struct dirent *dir;
    d = opendir(parent);
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            struct stat attrib;
            char folderName[200];
            strcpy(folderName, parent);
            strcat(folderName, "/");
            strcat(folderName, dir->d_name);
            if(stat(folderName, &attrib)==0){
                if (S_ISDIR(attrib.st_mode))
                {
                    if(isDirEmpty(folderName)){
                        sen = 1;
                        strcpy(res, folderName);
                        break;
                    }
                }
            }
        }
        closedir(d);
        return sen;
    }
}
//-------------------------------------------------------------
//Zip and delete dir function
int zipDir(char name[], char arg[]){
    //Calls exec in child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        printf("Zipping %s into %s (%d)\n", arg, name, getpid());
        char *argv[] = {"zip", "-rj", name, arg, NULL};
        execv("/usr/bin/zip", argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()");
                abort();
            }
        } while (status > 0);
    }

    return(0);
}

int delDir(char arg[]){

    //Calls exec in child process
    pid_t pid = fork();

    if (pid < 0) {
        printf("Error: Fork Failed\n");
        exit(1);
    }
    else if (pid == 0) {
        printf("Deleting %s (%d)\n", arg, getpid());
        char *argv[] = {"rm", "-rf", arg, NULL};
        execv("/usr/bin/rm", argv);
        exit(0);
    }
    else{
        int status;
        do {
            status = wait(NULL);
            if(status == -1 && errno != ECHILD) {
                perror("Error during wait()");
                abort();
            }
        } while (status > 0);
    }

    return(0);
}
//CURL EX
//-------------------------------------------------------------

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
  size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
  return written;
}

//---------------------------------------------------------------------

int main(int argc, char * argv[]){
    if(argc < 2){
        printf("ERROR : Usage is \"lukisan -(a or b)\"\n");
        return -1;
    }

    //Determines how "killer" kills the program
    int killtype;
    if(strcmp(argv[1], "-a")==0){
        killtype = 1;
    }
    else if(strcmp(argv[1], "-b")==0){
        killtype = 0;
    }
    else{
        printf("ERROR : Argument must be between -a or -b\n");
        return -1;
    }

    struct sigaction sa;
    sa.sa_handler = userHandler;

    

    pid_t pid, sid;        // Variabel untuk menyimpan PID

    //Stores program's directory absolute path
    char *path = realpath(".", NULL);
    char *ptr = realpath(".", NULL);
    char *path1 = malloc(100);
    strcpy(path1, path);
    strcat(path1, "/");
    
    pid = fork();     // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
    * (nilai variabel pid < 0) */
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
    * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0) {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    //Passes lukisan's pid to makefile
    pid_t mainPID = getpid();
    char mPID[sizeof(pid_t)];
    printf("lukisan.c's PID : %d\n", mainPID);
    sprintf(mPID, "%d", mainPID); 
    char * path2 = malloc(100);
    strcat(path2, path1);
    
    strcat(path1, MAKEFILE);
    mkdir(path1, 0777);
    strcat(path1, "/");
    strcat(path1, MAKEFILE);

    strcat(path2, KILLER);
    printf("ORI : %s\nPath1 : %s\nPath2 : %s\n", path, path1, path2);
    compileNew(path1, "#include <sys/types.h> \n\
#include <signal.h> \n\
#include <sys/stat.h> \n\
#include <stdio.h> \n\
#include <stdlib.h> \n\
#include <fcntl.h> \n\
#include <errno.h> \n\
#include <unistd.h> \n\
#include <syslog.h> \n\
#include <string.h> \n\
#include <time.h> \n\
#define DELAY 30 \n\
int main(int argc, char *argv[]){ \
 \
pid_t pid, sid;\
printf(\"ORIGINAL PID : %d\\n\", getpid());\
pid = fork();\
if (pid < 0) { \
  exit(EXIT_FAILURE); \
} \
 \
if (pid > 0) { \
  printf(\"CHILD PID = %d\\n\", pid); \
  exit(EXIT_SUCCESS); \
} \
umask(0); \
 \
sid = setsid(); \
if (sid < 0) { \
exit(EXIT_FAILURE); \
} \
 \
char * path = malloc(100);\
strcpy(path, argv[2]); \
 \
if ((chdir(path)) < 0) { \
exit(EXIT_FAILURE); \
} \
printf(\"kekw\\n\"); \
close(STDIN_FILENO); \n\
close(STDOUT_FILENO); \n\
close(STDERR_FILENO); \
 \
while(1){ \
    printf(\"CHILD CUR PID : %d\\n\", getpid()); \
 \
    struct tm *tm; \
    time_t t; \
    char str_time[100]; \
 \
    t = time(NULL); \
    tm = localtime(&t); \
 \
    strftime(str_time, sizeof(str_time), \"%F_%T\", tm); \
    strcat(path, \"/\"); \
    strcat(path, str_time); \
    if(!kill(atoi(argv[1]), 0)){ \
        printf(\"makefile.c : Main process is still running\\n\"); \
    } \
    else{ \
        if(errno == ESRCH){ \
          printf(\"makefile.c : Main process no longer running\\n\"); \
          exit(0); \
        } \
        else{ \
            printf(\"ERROR : makefile.c FAILED TO DETECT IF MAIN PROCESS IS STILL ACTIVE\\n\"); \
        } \
    } \
    if(mkdir(path ,0777) == -1){ \
      printf(\"ERROR CREATING DIRECTORY! : %s\\n\", strerror(errno)); \
      exit(0); \
    } \
    printf(\"OLD : %s\\n\", path);\
    free(path);\
    char * path = malloc(100);\
    strcpy(path, argv[2]); \
    sleep(DELAY); \
} \
return 0; \
}");
    sleep(1);
    runProg(path1, mPID, path);
    free(path1);
    char * code = malloc(1000);
    printf("%d %d\n", mainPID, killtype); 
    sprintf(code, "#define _XOPEN_SOURCE 700\n\
#include <stdio.h> \n\
#include <stdlib.h>\n\
#include <signal.h>\n\
#include <sys/types.h>\n\
#include <errno.h>\n\
#include <string.h>\n\
#include <unistd.h>\n\
\n\
int main(int argc, char *argv[]){\
    pid_t pid =%d;\
    int killType = %d;\
    int signal;\
    if(killType){\
        signal = SIGTERM;\
    }\
    else{\
        signal = SIGUSR2;\
    }\
    if(!kill(-(getpgid(pid)), signal)){\
        printf(\"Process KILLED.\\n\");\
        char selfpath[1000];\
        getcwd(selfpath, sizeof(selfpath));\
        pid_t selfpid = fork();\
        if(selfpid == 0){\
            strcat(selfpath, \"/killer\");\
            remove(selfpath);\
            return 0;\
        }\
        else{\
            return 0;\
        }\
    }\
    else{\
        if(errno == ESRCH){\
            printf(\"No such signal exists\\n\");\
        }\
        else{\
            printf(\"ERROR : Failed to kill program\\n\");\
        }\
    }\
    return 0;\
}", mainPID, killtype);
    printf("ke\n");
    compileNew(path2, code);
    wait(NULL);
    free(path2);
    free(code);

    //Curl setup
    pid_t child_id;
    CURL *curl_handle;
    FILE *pagefile;

    curl_global_init(CURL_GLOBAL_ALL);

    /* init the curl session */
    curl_handle = curl_easy_init();



    /* Switch on full protocol/debug output while testing */
    curl_easy_setopt(curl_handle, CURLOPT_VERBOSE, 1L);

    /* disable progress meter, set to 0L to enable it */
    curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, 1L);

    /* Enables redirects */
    curl_easy_setopt(curl_handle, CURLOPT_FOLLOWLOCATION, 1L);

    /* Change user agent to firefox, avoids "error 1020" due to curl being blocked*/
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/111.0");

    /* send all data to this function  */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_data);

    printf(">>PARENT GID : %d \n", getpgid(getpid()));

    //Generates child process when new dir is detected
    while(1){
        /* Generates empty dir */
        char * folderPath = malloc(200);
        if(getDir(ptr, folderPath)){
            child_id = fork();

                if (child_id < 0) {
                    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
                }

                if (child_id == 0) {
                    
                    strcpy(ptr, folderPath);
                    printf("FREE FOLDER : %s\nABSPATH : %s\n", folderPath, ptr);


                    for(int x = 0; x < AMOUNT; x++){
                        printf("CHILD PID : %d\n", getpid());
                        printf("CHILD GID : %d \n", getpgid(getpid()));
                        /* Generates link*/
                        printf("1 : %d\n", x);
                        char mainLink[] = "https://picsum.photos/";
                        char dim[6];
                        sprintf(dim, "%u", ((unsigned)time(NULL)%1000)+50);
                        strcat(mainLink, dim);
                        strcat(mainLink, ".jpg");
                        /* set URL to get here */
                        printf("2 : %d\n", x);
                        curl_easy_setopt(curl_handle, CURLOPT_URL, mainLink);
                        printf("3 : %d\n", x);
                        /* Gets datetime for file name */
                        struct tm *tm; 
                        time_t t; 
                        char str_time[100]; 
                    
                        t = time(NULL); 
                        tm = localtime(&t); 
                    
                        strftime(str_time, sizeof(str_time), "%F_%T", tm);
                        printf("4 : %d\n", x); 
                        /* Sets file name */
                        char * pagefilename = malloc(100);
                        strcpy(pagefilename, ptr);
                        strcat(pagefilename, "/");
                        strcat(pagefilename, str_time);
                        strcat(pagefilename,".jpg");
                        
                        printf("%d ||\nLINK : %s\nFILENAME : %s\n", x, mainLink, pagefilename);
                        /* open the file */
                        pagefile = fopen(pagefilename, "wb");
                        if(pagefile) {

                            /* write the page body to this file handle */
                            curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, pagefile);

                            /* get it! */
                            curl_easy_perform(curl_handle);

                            /* close the header file */
                            fclose(pagefile);
                        }
                        else{
                            printf("%s\n", strerror(errno));
                            exit(EXIT_FAILURE);
                        }
                        sleep(DELAY);
                    }

                    /* cleanup curl stuff */
                    curl_easy_cleanup(curl_handle);

                    curl_global_cleanup();

                    // /* Sets zip name */

                    char * zipName = malloc(200);
                    strcpy(zipName, ptr);
                    strcat(zipName,".zip");
                    zipDir(zipName, ptr);
                    delDir(ptr);
                    free(zipName);
                    free(folderPath);
                } else {
                    sleep(30);
                    sigaction(SIGUSR1, &sa, 0);
                    sigaction(SIGUSR2, &sa, 0);
                    printf("lukisan.c still running (%d)\n", getpid());
                    continue;
                }

        }
    }
    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>

#include <sys/stat.h>
#include <fcntl.h>
#include <syslog.h>
#define BASH "/usr/bin/bash"

#define ONESEC 1
#define ONEMIN 60
#define ONEHOUR 3600
#define ONEDAY 86400

void getDelay (time_t timeCur, time_t timeNext, float * res, int offSet){
  *res = difftime(timeNext, timeCur);
  if(*res < 0){
    *res = *res + offSet;
  }
}
//FORMAT : ./mainan HH MM SS {.sh file path}
//./mainan \* 41 1 /home/pelangimon/soal4/HelloWorld.sh
int isValidTime(long int valHour, long int valMin, long int valSec, int anyH, int anyM, int anyS){
  time_t rawtime;
  struct tm * timeinfo;

  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  if(!anyH){
    if(timeinfo->tm_hour != valHour){
      return 0;
    }
  }
  if(!anyM){
    if(timeinfo->tm_min != valMin){
      return 0;
    }
  }
  if(!anyS){
    if(timeinfo->tm_sec != valSec){
      return 0;
    }
  }
  return 1;
}

int execute(char * path){
    pid_t child_id;
  //Gets absolute path
    //printf("%s %s %s %s\n", argv[1], argv[2], argv[3], argv[4]);
  child_id = fork();

  if (child_id < 0) {
    exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
  }

  if (child_id == 0) {
    // this is child

    char *arg[] = {"bash", path};
    printf("%s %s\n", arg[0], arg[1]);
    printf("EXECV ERROR %d : %s\n", execv(BASH, arg), strerror(errno));
    exit(0);
  } 
  else{
    int status;
    do {
        status = wait(NULL);
        if(status == -1 && errno != ECHILD) {
            perror("Error during wait()");
            abort();
        }
    } while (status > 0);
  }
    
    return 0;
  
}

void instruction(){
  printf("FORMAT : ./mainan HH MM SS {.sh file path}\n");
}

int main(int argc, char * argv[]) {
  pid_t pid, sid;        // Variabel untuk menyimpan PID

  pid = fork();     // Menyimpan PID dari Child Process

  /* Keluar saat fork gagal
  * (nilai variabel pid < 0) */
  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  /* Keluar saat fork berhasil
  * (nilai variabel pid adalah PID dari child process) */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  //close(STDOUT_FILENO);
  close(STDERR_FILENO);

  printf("PID : %d\n", getpid());
  //Argument error
  if(argc < 5 || argc > 5){
    printf("ERROR : WRONG ARG AMOUNT %d\n", argc);
    for(int x = 0; x < argc; x++){
      printf("ARG %d : %s\n", x, argv[x]);
    }
    instruction();
    return 0;
  }
  //Determines crontab input format
  int anyHour, anyMin, anySec;
  long int hour, min, sec;
  char *end;
  anyHour = anyMin = anySec = 0;
  if(strcmp(argv[1], "*")==0){
    anyHour = 1;
  }
  else{
    if(strcmp(argv[1], "0")==0){
      hour = 0;
    }
    //ERROR CHECK
    //else if
    else{
      hour = strtol(argv[1], &end, 10);
      printf("HOUR INPUT : %ld\n", hour);
      if(hour < 0 || hour > 23){
        printf("ERROR : Invalid HOUR amount\n");
        instruction();
        return 0;
      }
    }
  }
  if(strcmp(argv[2], "*")==0){
    anyMin = 1;
  }
  else{
    if(strcmp(argv[2], "0")==0){
      min = 0;
    }
    //ERROR CHECK
    //else if
    else{
      min = strtol(argv[2], &end, 10);
      printf("MINUTE INPUT : %ld\n", min);
      if(min < 0 || min > 59){
      printf("ERROR : Invalid MINUTE amount\n");
      instruction();
      return 0;
      }
    }
  }
  if(strcmp(argv[3], "*")==0){
    anySec = 1;
  }
  else{
    if(strcmp(argv[3], "0")==0){
      sec = 0;
    }
    //ERROR CHECK
    //else if
    else{
      sec = strtol(argv[3], &end, 10);
      printf("SECOND INPUT : %ld\n", sec);
      if(sec < 0 || sec > 59){
      printf("ERROR : Invalid SECOND amount\n");
      instruction();
      return 0;
    }
    }
  }
  //Checks if file exists and can be accesed
  if (access(argv[4], F_OK) == 0){

  }
  else{
      printf("ERROR WHILE ACCESSING FILE : CODE %d (%s)\n", errno, strerror(errno));
      instruction();
      exit(EXIT_FAILURE);
  }
//Retrive current time and finds next valid trigger time
  time_t rawtime;
  struct tm * timeinfo;

  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  
/*
DETAILS ON EXECUTE AND DELAY STAGES
*/
  //Sets sleeping values
      //Delay stage
  int delayOff = 0;
    //Execute stage
  int repeat = 0;
  int delayOn = 0;

  if(!anyHour){
    delayOff = ONEDAY;
    if(!anyMin){
      if(!anySec){
        //HH MM SS
        repeat = 1;
        delayOn = 0;
      }
      else{
        //HH MM *
        repeat = 60;
        delayOn = 1;
      }
    }
    else{
      if(!anySec){
        //HH * SS
        repeat = 60;
        delayOn = 60;
      }
      else{
        //HH * *
        repeat = 3600;
        delayOn = 1;
      }
    }
  }
  else{
    if(!anyMin){
      delayOff = ONEHOUR;
      if(!anySec){
        //* MM SS
        repeat = 1;
        delayOn = 0;
      }
      else{
        //* MM *
        repeat = 60;
        delayOn = 1;
      }
    }
    else{
      repeat = 1;
      delayOn = 0;
      if(!anySec){
        //* * SS
        delayOff = ONEMIN;
      }
      else{
        //* * *
        delayOff = ONESEC;
      }
    }
  }
  
  //If enters in the middle of valid period, then firstExec = 1
  int firstExec = 1;
  time ( &rawtime );
  timeinfo = localtime ( &rawtime );
  struct tm ti= *timeinfo;
  float delay = 0;
  //Sets delay
  if(!anyHour){
      ti.tm_hour = hour;
      if(!anyMin){
        if(!anySec){
          //HH MM SS
          ti.tm_min = min;
          ti.tm_sec = sec;
          getDelay(rawtime, mktime(&ti), &delay, ONEDAY);
          sleep(delay);
        }
        else{
          //HH MM *
          if(timeinfo->tm_hour==hour && timeinfo->tm_min==min){

          }
          else{
            ti.tm_min = min;
            ti.tm_sec = 0;
            getDelay(rawtime, mktime(&ti), &delay, ONEDAY);


            sleep(delay);
          }
        }
      }
      else{
        if(!anySec){
          //HH * SS
          if(timeinfo->tm_hour!=hour){
            ti.tm_min = 0;
            ti.tm_sec = sec;
            getDelay(rawtime, mktime(&ti), &delay, ONEDAY);
            sleep(delay);
          }
          else{
            ti.tm_sec = sec;
            getDelay(rawtime, mktime(&ti), &delay, ONEMIN);
            sleep(delay);
          }
        }
        else{
          //HH * *
          if(timeinfo->tm_hour==hour){

          }
          else{
            ti.tm_min = 0;
            ti.tm_sec = 0;
            getDelay(rawtime, mktime(&ti), &delay, ONEDAY);


            sleep(delay);
          }
        }
      }
    }
    else{
      if(!anyMin){

        if(!anySec){
          //* MM SS
            ti.tm_min = min;
            ti.tm_sec = sec;
            getDelay(rawtime, mktime(&ti), &delay, ONEHOUR);
            sleep(delay);
        }
        else{
          //* MM *
          if(timeinfo->tm_min==min){

          }
          else{
            ti.tm_min = min;
            ti.tm_sec = 0;
            getDelay(rawtime, mktime(&ti), &delay, ONEHOUR);
            sleep(delay);
          }
        }
      }
      else{

        if(!anySec){
          //* * SS
            ti.tm_sec = sec;
            getDelay(rawtime, mktime(&ti), &delay, ONEMIN);
            sleep(delay);
        }
        else{
          //* * *

        }
      }
    }
  //Enters loop 
  //CASE 4 : During valid time
  
  if(!delayOn){
    firstExec = 0;
  }

  while(1){
  //DEBUG

  //Execute
  
  printf("repeat = %d\ndelayOn = %d\ndelayOff = %d\n", repeat, delayOn, delayOff);
  for(int x = 0; x < repeat; x++){
    
    execute(argv[4]);
      //Detects if still valid time
    if(firstExec){
      if(!isValidTime(hour, min, sec, anyHour, anyMin, anySec)){
        firstExec = 0;
        break;
      }
    }
    sleep(delayOn);
  }
  //Delay
  sleep(delayOff);
  }
  return 0;
}

#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

int main() {

    pid_t child;

    char *url = "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq";
    char *downloadName = "binatang.zip";
    int cek;

    printf("\n\n-- SOAL A --\n");

    // melakukan pendownloadan file zip
    child = fork();
    if (child == 0) {
        char *args[] = {"wget", "-O", downloadName, url, NULL};
        execv("/bin/wget", args);
    }
    waitpid(child, &cek, 0);

    // Mengunzip File
    child = fork();
    if (child == 0) {
        char *args[] = {"unzip", downloadName, NULL};
        execv("/bin/unzip", args);
    }
    waitpid(child, &cek, 0);

    printf("\n\n-- SOAL B --\n");

    // Memilih gambar secara acak
    DIR *dir;
    struct dirent *ent;
    
    char *dir_name = ".";
    char **files = NULL;
    int gambar = 0;
    // Membuka direktori
    dir = opendir(dir_name);

    // Melakukan perhitungan jumlah gambar
    while ((ent = readdir(dir)) != NULL) {
        char *extension = strrchr(ent->d_name, '.');
        if (extension != NULL && (strcmp(extension, ".jpg") == 0)) {
            gambar++;
        }
    }

    files = malloc(sizeof(char *) * gambar);
    rewinddir(dir);

    int i = 0;
    while ((ent = readdir(dir)) != NULL) {
        char *extension = strrchr(ent->d_name, '.');
        if (extension != NULL && (strcmp(extension, ".jpg") == 0)) {
            files[i] = strdup(ent->d_name);
            i++;
        }
    }

    closedir(dir);

    // Melakukan pemilihan gambar secara random
    if (gambar > 0) {
        srand(time(NULL));
        int index = rand() % gambar;
        printf("Gambar random yang terpilih : %s\n", files[index]);
    }

    // free array of filenames
    for (int i = 0; i < gambar; i++) {
        free(files[i]);
    }

    free(files);

    printf("\n-- SOAL C--\n");

    // Inisiasi pembuatan folder
    char *air = "HewanAir";
    char *darat = "HewanDarat";
    char *amphibi = "HewanAmphibi";
    
    if (mkdir(darat, 0777) == -1 || mkdir(amphibi, 0777) == -1 || mkdir(air, 0777) == -1) {
        fprintf(stderr, "Error: %s\n", strerror(errno));
    } else {
        printf("Directori succes\n");
    }

    // melakukan pemindahan file ke subdirektori
     char path_akhir[400];
    char path_awal[400];
    char unique_name[400];
    char target_dir[400];
    struct stat st;
    dir = opendir(".");

    // melakukan filtering file pada direktori
    while ((ent = readdir(dir)) != NULL) {
        sprintf(path_awal, "./%s", ent->d_name);

        // melakukan skip file yang tersembunyi
        if (ent->d_type == DT_DIR || ent->d_name[0] == '.') {
            continue;
        }
        // file amphibi
        else if (strstr(ent->d_name, "_amphibi") != NULL) {
            sprintf(path_akhir, "./HewanAmphibi/%s", ent->d_name);

            // pemindahan ke path baru
            rename(path_awal, path_akhir);
        }
       

        // file darat
        else if (strstr(ent->d_name, "_darat") != NULL) {
            sprintf(path_akhir, "./HewanDarat/%s", ent->d_name);

            // pemindahan ke path baru
            rename(path_awal,path_akhir);
        }

         // file air
        if (strstr(ent->d_name, "_air") != NULL) {
            sprintf(path_akhir, "./HewanAir/%s", ent->d_name);

            // pemindahan ke path baru
            rename(path_awal, path_akhir);
        }

        else {
            continue;
        }
    }

    printf("\n-- SOAL D --\n");

    // melakukan zzip file ke directori yang di tentukan
    child = fork();
    if (child == 0) {
        char *args[] = {"zip", "-r", "HewanDarat.zip", "HewanDarat/", NULL};
        execv("/bin/zip", args);
    }
    waitpid(child, &cek, 0);

    child = fork();
    if (child == 0) {
        char *args[] = {"zip", "-r", "HewanAir.zip", "HewanAir/", NULL};
        execv("/bin/zip", args);
    }
    waitpid(child, &cek, 0);

    child = fork();
    if (child == 0) {
        char *args[] = {"zip", "-r", "HewanAmphibi.zip", "HewanAmphibi/", NULL};
        execv("/bin/zip", args);
    }
    waitpid(child, &cek, 0);

    // Menghilangkan directori yang tidak ter unzip
    child = fork();
    if (child == 0) {
        char *args[] = {"rm", "-r", "HewanDarat/", NULL};
        execv("/bin/rm", args);
    }
    waitpid(child, &cek, 0);

    child = fork();
    if (child == 0) {
        char *args[] = {"rm", "-r", "HewanAir/", NULL};
        execv("/bin/rm", args);
    }
    waitpid(child, &cek, 0);

    child = fork();
    if (child == 0) {
        char *args[] = {"rm", "-r", "HewanAmphibi/", NULL};
        execv("/bin/rm", args);
    }
    waitpid(child, &cek, 0);

    closedir(dir);
    return 0;
}
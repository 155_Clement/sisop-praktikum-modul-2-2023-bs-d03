#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define zip "players.zip"
#define link "https://drive.google.com/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download"

void buatTim(int bek, int gelandang, int striker)
{
  int kiper = 1;
  // buat file formasi
  char filecreate[254];
  sprintf(filecreate, "/home/navahindra/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
  pid_t c_file = fork();
  if (c_file == 0)
  {
    char *args[] = {"touch", filecreate, NULL};
    execvp(args[0], args);
  }
  else
  {
    int status;
    wait(&status);
  }

  // list kiper
  pid_t writing_kiper = fork();
  if (writing_kiper == 0)
  {
    pid_t child = fork();
    if (child == 0)
    {
      sprintf(filecreate, "echo 'Kiper:' >> /home/navahindra/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
    else
    {
      int status;
      wait(&status);
      sprintf(filecreate, "ls -1 players/Kiper/*.png | sort -n -r -t _ -k 4 | sed 's|players/Kiper/||g' | head -n %d >> /home/navahindra/Formasi_%d-%d-%d.txt", kiper, bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
  }
  else
  {
    int status;
    wait(&status);
  }

  // list bek
  pid_t writing_bek = fork();
  if (writing_bek == 0)
  {
    pid_t child = fork();
    if (child == 0)
    {
      sprintf(filecreate, "echo 'Bek:' >> /home/navahindra/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
    else
    {
      int status;
      wait(&status);
      sprintf(filecreate, "ls -1 players/Bek/*.png | sort -n -r -t _ -k 4 | sed 's|players/Bek/||g' | head -n %d >> /home/navahindra/Formasi_%d-%d-%d.txt", bek, bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
  }
  else
  {
    int status;
    wait(&status);
  }

  // list gelandang
  pid_t writing_gelandang = fork();
  if (writing_gelandang == 0)
  {
    pid_t child = fork();
    if (child == 0)
    {
      sprintf(filecreate, "echo 'Gelandang:' >> /home/navahindra/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
    else
    {
      int status;
      wait(&status);
      sprintf(filecreate, "ls -1 players/Gelandang/*.png | sort -n -r -t _ -k 4 | sed 's|players/Gelandang/||g' | head -n %d >> /home/navahindra/Formasi_%d-%d-%d.txt", gelandang, bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
  }
  else
  {
    int status;
    wait(&status);
  }

  // list striker
  pid_t writing_striker = fork();
  if (writing_striker == 0)
  {
    pid_t child = fork();
    if (child == 0)
    {
      sprintf(filecreate, "echo 'Striker:' >> /home/navahindra/Formasi_%d-%d-%d.txt", bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
    else
    {
      int status;
      wait(&status);
      sprintf(filecreate, "ls -1 players/Penyerang/*.png | sort -n -r -t _ -k 4 | sed 's|players/Penyerang/||g' | head -n %d >> /home/navahindra/Formasi_%d-%d-%d.txt", striker, bek, gelandang, striker);
      char *args[] = {"sh", "-c", filecreate, NULL};
      execvp(args[0], args);
    }
  }
  else
  {
    int status;
    wait(&status);
  }
}

int main()
{
  pid_t child_id;

  child_id = fork();

  if (child_id == 0)
  {
    pid_t grand_child;
    grand_child = fork();
    if (grand_child == 0)
    {
      printf("Downloading.....\n\n");
      execl("/usr/bin/wget", "/usr/bin/wget", link, "-O", zip, NULL);
    }
    else
    {
      int status;
      wait(&status);
      printf("Unzipping.....\n\n");
      execl("/usr/bin/unzip", "/usr/bin/unzip", zip, "-d", ".", NULL);
    }
  }
  else
  {
    int status;
    wait(&status);
  }

  // removing zip
  printf("removing zip file...\n\n");
  pid_t ziprm = fork();
  if (ziprm == 0)
  {
    char *args[] = {"rm", "players.zip", NULL};
    execvp("rm", args);
  }
  else
  {
    int status;
    wait(&status);
  }

  // remove anything exept ManUtd
  pid_t remove = fork();
  if (remove == 0)
  {
    char *args[] = {"find", "players", "-maxdepth", "1", "-not", "-name", "*ManUtd*", "-type", "f", "-delete", NULL};
    execvp("find", args);
  }
  else
  {
    int status;
    wait(&status);
  }

  printf("Making folders...\n\n");
  pid_t makedir = fork();
  if (makedir == 0)
  {
    char *args[] = {"mkdir", "players/Kiper", "players/Gelandang", "players/Penyerang", "players/Bek", NULL};
    execvp("mkdir", args);
  }
  else
  {
    int status;
    wait(&status);
  }

  printf("Classifying...\n\n");
  pid_t child1 = fork();
  if (child1 == 0)
  {
    pid_t child2 = fork();
    if (child2 == 0)
    {
      pid_t child3 = fork();
      if (child3 == 0)
      {
        pid_t child4 = fork();
        if (child4 == 0)
        {
          execl("/bin/sh", "sh", "-c", "mv players/*Bek* players/Bek/ 2>/dev/null", NULL);
        }
        else
        {
          execl("/bin/sh", "sh", "-c", "mv players/*Gelandang* players/Gelandang/ 2>/dev/null", NULL);
        }
      }
      else
      {
        execl("/bin/sh", "sh", "-c", "mv players/*Penyerang* players/Penyerang/ 2>/dev/null", NULL);
      }
    }
    else
    {
      execl("/bin/sh", "sh", "-c", "mv players/*Kiper* players/Kiper/ 2>/dev/null", NULL);
    }
  }
  else
  {
    int status;
    wait(&status);
  }

  int b, g, s;
  while (1)
  {
    printf("Masukkan urutan formasi(Bek, Gelandang, Penyerang): \n");
    scanf("%d %d %d", &b, &g, &s);
    // exception jika pemain yang diinput kurang dari 10
    if ((b + g + s) < 10)
    {
      printf("Jumlah pemain dalam formasi kurang!\n");
    }
    else if ((b + g + s) > 10)
    {
      printf("Jumlah pemain dalam formasi kelebihan!\n");
    }
    else
    {
      break;
    }
  }
  buatTim(b, g, s);

  printf("Done\n");
  return 0;
}
